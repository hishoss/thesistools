from django.shortcuts import render
from .forms import PlagDocForm
from django.contrib.auth.decorators import login_required
from builtins import object
import sys
import os
import pickle

from plagcomps.intrinsic import get_plagiarism_passages
from plagcomps.shared import util
from os.path import basename


# Create your views here.
# ------------------------------------------------------------
media_root = 'run/media/'

class PlagDetector(object):
    '''
    Class used (for the moment) to interact with the detection
    module. Should be removed later once the detection module
    has been nicely packaged.
    '''

    def get_passages(self, atom_type, features, cluster_method, k, filename=None):
        if filename == 'pickle':
            pickle_file = os.path.join(media_root, 'sample_docs/passages.dat')
            with open(pickle_file) as f:
                passages = pickle.load(f)
                f.close()

            return passages
        elif filename is None:
            filename = os.path.join(media_root, 'sample_docs/head_training_sample.txt')

        u = util.IntrinsicUtility()
        with open(filename) as f:
            content = f.read()
            f.close()

        passages = get_plagiarism_passages(content, atom_type, features, cluster_method, k)

        # Look for a corresponding XML file and add ground truth if one exists
        xml_path = filename.replace('.txt', '.xml')
        if os.path.exists(xml_path):
            u.add_ground_truth_to_passages(passages, xml_path)

        return passages

    def get_ground_truth_passages(self, atom_type, file_path, xml_path):
        passages = util.BaseUtility().get_bare_passages_and_plagiarized_spans(file_path, xml_path, atom_type)

        return passages


def plag_home(request):
    return render(request, 'plag_home.html', {})


def plag_about(request):
    return render(request, 'plag_about.html', {})


# @app.route('/about/')
# def about():
#     return render_template('about.html')

features = [
    # 'average_word_length',
    'average_sentence_length',
    'stopword_percentage',
    'punctuation_percentage',
    'syntactic_complexity',
    'avg_internal_word_freq_class',
    'avg_external_word_freq_class',
]

atom_type = 'paragraph'
k = 2
cluster_method = 'hmm'

@login_required
def select_doc(request):
    if request.method == "POST":
        form = PlagDocForm(request.POST, request.FILES)

        if form.is_valid():
            plag_doc = form.save(commit=False)
            # plag_doc.doc = media_root + plag_doc.doc

            plag_doc.save()
            print('form valid')

            # atom_type = plag_doc.atom
            # features = plag_doc.features
            # cluster_method = plag_doc.cluster_method
            # k = plag_doc.k
            doc_name = basename(str(plag_doc.doc))
            full_path = media_root + str(plag_doc.doc)

            all_passages = PlagDetector().get_passages(atom_type, features, cluster_method, k, full_path)
            feature_names = list(all_passages[0].features.keys())
            return render(request, 'view_doc.html', {
                                                   'atom_type': atom_type,
                                                   'cluster_method': cluster_method,
                                                   'k': k,
                                                   'passages': all_passages,
                                                   'features': feature_names,
                                                   'doc_name': doc_name,
            })
        # elif len(form.errors) > 0:
        #     # TODO Make a nice error msg
        #     return str(form.errors)
    else:
        # Otherwise display options
        print('form invalid')
        form = PlagDocForm()
    return render(request, 'select_doc.html', {'form': form})


@login_required
def view_source_doc(request):
    full_path = media_root + 'sample_docs/test2.txt'
    xml_path = media_root + 'sample_docs/test2.xml'

    atom_type = 'paragraph'
    passages = PlagDetector().get_ground_truth_passages(atom_type, full_path, xml_path)

    return render(request, 'view_source_doc.html', {
                                                    'passages': passages,
                                                    'atom_type': atom_type,
    })


def show_sample(request):
    '''
    Use a pickled file of passage objects parsed from static/training_sample.txt
    to sample the front-end
    '''
    # all_passages = PlagDetector().get_passages('pickle')
    all_passages = PlagDetector().get_passages()
    features = list(all_passages[0].features.keys())

    return render(request, 'view_doc.html', {
                                             'doc_name': 'Sample',
                                             'passages': all_passages,
                                             'features': features
    })