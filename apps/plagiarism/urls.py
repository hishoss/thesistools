from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.plag_home, name='plag_home'),
    url(r'^about/$', views.plag_about, name='plag_about'),
    url(r'^sample/$', views.show_sample, name='show_sample'),
    url(r'^test/$', views.view_source_doc, name='view_source_doc'),
    url(r'^upload/$', views.select_doc, name='select_doc'),
]

