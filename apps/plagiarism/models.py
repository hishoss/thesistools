from django.db import models
from formatting.models import Thesis

atoms = [
    ('paragraph', 'paragraph'),
    ('sentence', 'sentence'),
    ('word', 'word'),
    ]

cluster = [
    ('kmeans', 'kmeans'),
    ('agglom', 'agglom'),
    ('hmm', 'hmm'),
    ('none', 'none'),
]

features = [
    ('average_word_length', 'average_word_length'),
    ('average_sentence_length', 'average_sentence_length'),
    ('stopword_percentage', 'stopword_percentage'),
    ('punctuation_percentage', 'punctuation_percentage'),
    ('syntactic_complexity', 'syntactic_complexity'),
    ('avg_internal_word_freq_class', 'avg_internal_word_freq_class'),
    ('avg_external_word_freq_class', 'avg_external_word_freq_class'),
]
# try zipping these

ints = [
    (2, '2'),
    (3, '3'),
    (4, '4'),
    (5, '5'),
]


class PlagDoc(models.Model):
    thesis = models.OneToOneField(Thesis, unique=True, verbose_name=('thesis'),
                                  related_name='plag_thesis', null=True, blank=True)
    # doc_name = models.FileField(upload_to='plagiarism/%Y/%m/%d/', default='') # add max size
    doc = models.FileField(upload_to='documents/%Y/%m/%d/', default="", null=True)
    features = models.CharField(max_length=30, choices=features, default='average_sentence_length')
    atom = models.CharField(max_length=10, choices=atoms, default='paragraph')
    cluster_method = models.CharField(max_length=6, choices=cluster, default='kmeans')
    k = models.PositiveSmallIntegerField(choices=ints, default=2)

    # def __str__(self):
    #     return self.thesis.title






#
# atoms = [
#     ('P', 'paragraph'),
#     ('S', 'sentence'),
#     ('W', 'word'),
#     ]
#
# cluster = [
#     ('KM', 'kmeans'),
#     ('AG', 'agglom'),
#     ('HM', 'hmm'),
#     ('NN', 'none'),
# ]
#
# features = [
#     ('AWL', 'average_word_length'),
#     ('ASL', 'average_sentence_length'),
#     ('STP', 'stopword_percentage'),
#     ('PNP', 'punctuation_percentage'),
#     ('SYC', 'syntactic_complexity'),
#     ('AIWFC', 'avg_internal_word_freq_class'),
#     ('AEWFC', 'avg_external_word_freq_class'),
# ]
