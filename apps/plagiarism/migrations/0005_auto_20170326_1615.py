# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-26 16:15
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('plagiarism', '0004_auto_20170326_1400'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plagdoc',
            name='atom',
            field=models.CharField(choices=[('paragraph', 'paragraph'), ('sentence', 'sentence'), ('word', 'word')], default='paragraph', max_length=1),
        ),
        migrations.AlterField(
            model_name='plagdoc',
            name='cluster_method',
            field=models.CharField(choices=[('kmeans', 'kmeans'), ('agglom', 'agglom'), ('hmm', 'hmm'), ('none', 'none')], default='kmeans', max_length=2),
        ),
        migrations.AlterField(
            model_name='plagdoc',
            name='doc',
            field=models.FileField(default='', null=True, upload_to='documents/%Y/%m/%d/'),
        ),
        migrations.AlterField(
            model_name='plagdoc',
            name='features',
            field=models.CharField(choices=[('average_word_length', 'average_word_length'), ('average_sentence_length', 'average_sentence_length'), ('stopword_percentage', 'stopword_percentage'), ('punctuation_percentage', 'punctuation_percentage'), ('syntactic_complexity', 'syntactic_complexity'), ('avg_internal_word_freq_class', 'avg_internal_word_freq_class'), ('avg_external_word_freq_class', 'avg_external_word_freq_class')], default='average_sentence_length', max_length=5),
        ),
    ]
