from django import forms
from .models import PlagDoc


features = [
    'average_word_length',
    'average_sentence_length',
    'stopword_percentage',
    'punctuation_percentage',
    'syntactic_complexity',
    'avg_internal_word_freq_class',
    'avg_external_word_freq_class',
]



class PlagDocForm(forms.ModelForm):
    class Meta:
        model = PlagDoc
        # fields = ('thesis', 'doc', 'features', 'atom', 'cluster_method', 'k')
        # fields = ('doc', 'features', 'atom', 'cluster_method', 'k')
        fields = ('doc',)

    # def __init__(self, *args, **kwargs):
    #     super(PlagDocForm, self).__init__(*args, **kwargs)
    #     self.fields['features'] = forms.ModelChoiceField(queryset=None, choices=features,
    #                                                      empty_label="Features")


#
# class ChoiceForm(ModelForm):
#     class Meta:
#         model = YourModel
#
#     def __init__(self, *args, **kwargs):
#         super(ChoiceForm, self).__init__(*args, **kwargs)
#         self.fields['countries'] =  ModelChoiceField(queryset=YourModel.objects.all()),
#                                              empty_label="Choose a countries",)
