from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from userena.models import UserenaBaseProfile
from formatting.models import Thesis


# Create your models here.

class UserProfile(UserenaBaseProfile):
    user = models.OneToOneField(User,
                                unique=True,
                                verbose_name=_('user'),
                                related_name='my_profile')
    # thesis = models.ForeignKey(Thesis)
    thesis = models.OneToOneField(Thesis,
                                  unique=True,
                                  verbose_name=_('thesis'),
                                  related_name='my_thesis',
                                  null=True,
                                  blank=True)
    favourite_snack = models.CharField(_('favourite snack'),
                                       max_length=5)

