from django.shortcuts import render
from accounts.models import UserProfile
from .forms import ThesisForm
from django.contrib.auth.decorators import login_required
from django.utils import timezone
import pypandoc as pyd
import textract
import shutil
import ntpath
from os.path import abspath, basename, dirname, join, normpath





# ---------------- Helper funcs --------------------------------------------------------------
media_root = 'run'


def cc(doc, toFormat):
    dirr = doc.split('.')[0]

    filters = ['pandoc-citeproc']
    pydoc_args = ['-s', '--latexmathml',  '--extract-media=' + dirr]

    file_name = dirr.split('/')[-1]
    out = dirr + '/' + file_name + '.' + toFormat
    pyd.convert_file(doc, to=toFormat, extra_args=pydoc_args,
                     filters=filters, outputfile=out)

    shutil.make_archive(dirr, 'zip', dirr)
    # shutil.rmtree(dirr)
    return dirr + '.zip'


def priority_to_time(priority):
    t = "Thanks For using our service. We'll get back to you in "
    d = {1: '6 hours',
         2: '24 hours',
         3: '72 hours'}
    return t + d[priority]
# put this in the template to indicate ETA to download
# {{ conference_date|timeuntil:from_date }}


def content_type(doc):
    ext = doc.content_type.split('/')[1]
    if ext == 'vnd.openxmlformats-officedocument.wordprocessingml.document':
        return 'docx'
    return ext


def content_type1(doc):
    docx = 'vnd.openxmlformats-officedocument.wordprocessingml.document'
    allowed = [docx,
               'tex',
               'html',
               ]
    ext = doc.content_type.split('/')[1]

    if ext not in allowed : return -1
    if ext == docx: return docx
    return ext


# -------------------------------------------------------------------------------------------

@login_required
def convert(request):
    return render(request, 'convert.html', {})


@login_required
def formatting(request):
        if request.method == "POST":
            form = ThesisForm(request.POST, request.FILES)
            doc = request.FILES['document']
            # ext = content_type(doc)

            ext = content_type(doc)
            if ext == -1:
                return render(request, 'convert.html',
                            {'uploadMessage': "There's a problem with the file"})

            # doc_name = doc.name.split('.')[0]
            # doc.size  -- do some check on this
            if form.is_valid():
                thesis_obj = form.save(commit=False)

                # Need to fix saving the obj first. THis error happens because
                # before the file is saved it's path is just the file name
                # but afterwards, it becomes the document/.../<file_name>
                # If this path can be generated before saving, it'll avoid
                # problematic files from being saved into the database
                # print('**********************')
                # print(thesis_obj.document.url)
                thesis_obj.save()
                # print(thesis_obj.document.url)
                # print('**********************')
                thesis_obj.input_format = ext
                thesis_obj.date_uploaded = timezone.now()
                try:
                    output = cc(media_root + thesis_obj.document.url, thesis_obj.output_format)
                except RuntimeError:
                    return render(request, 'convert.html',
                            {'uploadMessage': "There's a problem with the file"})

                thesis_obj.convertedOF = output
                thesis_obj.save()

                # get user object
                user = UserProfile.objects.get(user__exact=request.user)
                user.thesis = thesis_obj
                urgency = priority_to_time(thesis_obj.urgency)
                user.save()

                return render(request, 'convert.html', {'uploadMessage': urgency})
        else:
            form = ThesisForm()
        # return render(request, 'upload.html', {'form': form})
        return render(request, 'formatting.html', {'form': form})
        # formatting.html and upload.html are the same. need to purge upload.html


def to_txt(doc):
    new_name =doc.split('.')[0] + '.txt'
    text = textract.process(doc, method='tesseract', encoding='ascii')
    with open(new_name, 'wb') as f:
        f.write(text)
        f.close()
