from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


# ---------------------------------------------
formats = [('docx', 'Microsoft Document'),
           ('html', 'HTML5'),
           ('tex', 'LaTex'),
           ]
levels = [('UG', 'Undergraduate'),
          ('PG', 'Post Graduate'),
          ('GR', 'Graduate'),
          ]

priority = [(1, 'High'),
            (2, 'Medium'),
            (3, 'Low'),
            ]

# ---------------------------------------------


# Create your models here.
class Thesis(models.Model):
    title = models.CharField(max_length=200)
    department = models.CharField(max_length=50)
    level = models.CharField(max_length=10, choices=levels, default='UG')
    urgency = models.PositiveSmallIntegerField(choices=priority, default=3)
    document = models.FileField(upload_to='documents/%Y/%m/%d/', default='') # add max size
    convertedOF = models.FileField(default="", null=True) # converted output format
    convertedTXT = models.FileField(default="", null=True) # converted txt for plagiarism detection
    uploaded_at = models.DateTimeField(default=timezone.now)
    input_format = models.CharField(max_length=5)
    output_format = models.CharField(default='tex', max_length=5, choices=formats)
    comments = models.TextField(blank=True)

    def __str__(self):
        return self.title
