from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.formatting, name='formatting'),
    url(r'^formatting/convert/$', views.convert, name='convert')
]

